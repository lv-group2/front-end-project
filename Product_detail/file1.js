const btnop1 = document.getElementById("op_1");
const btnop2 = document.getElementById("op_2");
const op1Text = document.getElementById("op-1-text");
const op2Text = document.getElementById("op-2-text");
const logoutbutton = document.getElementById("logout-btn");
const content=document.getElementById("myDIV");


btnop1.onclick = function () {
    op1Text.style.display = "block";
    op2Text.style.display = "none";
    btnop1.style.borderBottom = "2px solid rgb(92, 206, 252)";
    btnop2.style.borderBottom = "none";
    btnop1.style.transition = "0.3s ease-in"
}

btnop2.onclick = function () {
    op1Text.style.display = "none";
    op2Text.style.display = "block";
    btnop1.style.borderBottom = "none";
    btnop2.style.borderBottom = "2px solid rgb(92, 206, 252)";
    btnop2.style.transition = "0.3s ease-in";
}

logoutbutton.onclick = function () {
    if (content.style.display == "none") {
        content.style.display = "block";
    }else{
        content.style.display = "none";
    }
}
